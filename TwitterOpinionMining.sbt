lazy val root = (project in file(".")).
  settings(
    name := "TwitterOpinionMining",
    version := "1.0",
    scalaVersion := "2.10.4",
    mainClass in Compile := Some("TwitterOpinionMining")        
  )

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % "1.6.1" % "provided",
  "org.apache.spark" %% "spark-sql" % "1.6.1" % "provided",
  "org.apache.spark" %% "spark-hive" % "1.6.1" % "provided",
  "org.apache.spark" %% "spark-mllib" % "1.6.1" % "provided",
  "org.apache.spark" %% "spark-streaming" % "1.6.1" % "provided",
  "org.apache.spark" % "spark-streaming-kafka_2.10" % "1.6.1",
  "org.apache.kafka" % "kafka_2.10" % "0.8.2",
  "au.com.bytecode" % "opencsv" % "2.4", 
  "com.databricks" % "spark-csv_2.10" % "1.4.0",
  "com.typesafe" % "config" % "1.3.0"
)

// META-INF discarding
mergeStrategy in assembly <<= (mergeStrategy in assembly) { (old) =>
   {
    case PathList("META-INF", xs @ _*) => MergeStrategy.discard
    case x => MergeStrategy.first
   }
}

assemblyJarName in assembly := "twitter_opinion_mining.jar"
